const express = require('express');
const router = express.Router();
const h = require('../handlers');
const storage = require('../utils/storage');

// authentication
router.post('/auth/login', h.auth.login);
router.post('/auth/register', h.auth.register);
router.get('/auth/login/google', h.auth.google);
router.get('/auth/login/facebook', h.auth.facebook);

// upload video media handling
router.post('/upload', storage.single('media'), (req, res) => {
    const videoUrl = req.protocol + '://' + req.get('host') + '/videos/' + req.file.filename;

    return res.json({
        videoUrl
    });
});

module.exports = router;