require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const app = express();
const router = require('./routes');

app.use(express.json());
app.use('/api', router);
app.use(morgan('dev'));
app.use('/videos', express.static('public/videos'));

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log('listening on port', PORT));