const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, './public/videos');
    },

    // generete unique filename
    filename: (req, file, callback) => {
        const namaFile = Date.now() + path.extname(file.originalname);
        callback(null, namaFile);
    }
});

const upload = multer({
    storage: storage,

    fileFilter: (req, file, callback) => {
        if (file.mimetype == 'video/webm' || file.mimetype == 'video/mp4') {
            callback(null, true);
        } else {
            const err = new Error('only webm, mp4, allowed to upload!');
            callback(err, false);
        }
    },
    onError: (err, next) => {
        next(err);
    }
})


module.exports = upload