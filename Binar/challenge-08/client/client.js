const VAPID_PUBLIC_KEY = 'BFN7AodrnWuIs4BVOVwHIPMY-0BAeI07o5O8p-ISGvyeDVv1zCqm1eVGhxe86_XhT-kMn_ySs6uh_i9buTTn3_8';

// konfirmasi subscribe notif
async function notifPermission() {
    return new Promise(async (resolve) => {
        resolve(await Notification.requestPermission());
    });
}

// check apakah browser support untuk service workers
function isServiceWorkerSupported() {
    if ('serviceWorker' in navigator) return true;

    return false;
}

async function registerServiceWorker() {
    if (isServiceWorkerSupported) {
        let permission = Notification.permission; // default, granted, denied

        if (permission === 'default') {
            permission = await notifPermission();
        } else {
            return;
        }

        if (permission == 'denied') return console.log('notif denied');


        console.log('ok');
        // register service worker
        const register = await navigator.serviceWorker.register('./worker.js', {
            scope: '/'
        });

        const subscription = await register.pushManager.subscribe({
            userVisibleOnly: true,
            applicationServerKey: VAPID_PUBLIC_KEY,
        });

        await fetch("/auth/register", {
            method: "POST",
            body: JSON.stringify(subscription),
            headers: {
                "Content-Type": "application/json",
            }
        });
    } else {
        return alert('Push notification nor supported!');
    }
}
registerServiceWorker();