const os = require('os');
const fs = require('fs');
const person = require('./person.json')

// import luas segitiga
const luasSegitiga = require('./helper.js');
console.log('Free Memory:', os.freemem());
console.log(luasSegitiga(3, 4));

// read file
const isi = fs.readFileSync('./test.txt', 'utf-8');
console.log(isi);

// write file
const isiBaru = 'Ini adalah isi terbaru';
fs.writeFileSync('./test.txt', isiBaru);



function createPerson(person) {
    fs.writeFileSync('./person.json', JSON.stringify(person));
    return person;
}

const sabrina = createPerson({
    name: 'Sabrina',
    age: 29
});

console.log(sabrina);